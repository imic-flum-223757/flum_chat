import 'package:rxdart/rxdart.dart';

class MessageBloc{
  MessageBloc._internal();
  static final MessageBloc _instance = MessageBloc._internal();
  static MessageBloc get instance {
    return _instance;
  }
  final _message = BehaviorSubject<Map<String, dynamic>>();
  Stream<Map<String, dynamic>> get messageStream => _message.stream;

  void addMessage(Map<String, dynamic> msg) {
    _message.add(msg);
    print('go here!');
  }


  void dispose() {
    _message.close();
  }
}