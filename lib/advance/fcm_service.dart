import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

import 'message_bloc.dart';

class FCMService {
  FCMService._internal() {
    _fcmMessaging = FirebaseMessaging();
    fcmCloudMessagingListeners();
  }

  static final FCMService _instance = FCMService._internal();
  // final PrefsServices _prefs = PrefsServices();
  // final OdooApiService _service = OdooApiService.instance;

  static FCMService get instance {
    return _instance;
  }

//  Api api = Api.instance;

  // get the message stream
  MessageBloc _messageStream = MessageBloc.instance;
  FirebaseMessaging _fcmMessaging;

  // getter for firebase messaging client
  get fcmMessaging => _fcmMessaging;

  Future<String> getToken() async => await _fcmMessaging.getToken();

  // method for getting the messaging token
  void sendDeviceToken() {
    _fcmMessaging.getToken().then((token) async {
      print('=========================');
      print("MESSAGING TOKEN: " + token);
      // _service.updateFbToken(token);
      // Kiểm tra xem user có phải public user không
      // Nếu không => check xem user.token có = token không
      // Nếu không => gửi

// Không cần lưu vào shareReference
//      String tokenSave = await _prefs.getFCMToken();
//      if (tokenSave == '' || tokenSave != token) {
//        _prefs.saveFCMToken(token);
//       }
    });
  }

  void fcmSubscribe({String topic = 'customer'}) {
    _fcmMessaging.subscribeToTopic('$topic');
  }

  void fcmUnSubscribe({String topic = 'customer'}) {
    _fcmMessaging.unsubscribeFromTopic('$topic');
  }

  void fcmCloudMessagingListeners() {
    if (Platform.isIOS) getIOSPermission();

    _fcmMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        // add message to stream
        _messageStream.addMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        _messageStream.addMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        _messageStream.addMessage(message);
      },
    );
  }

  void getIOSPermission() {
    _fcmMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _fcmMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}
